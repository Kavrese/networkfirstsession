package com.cit.networkfirstsession

data class ModelError(
    val error: String
)
