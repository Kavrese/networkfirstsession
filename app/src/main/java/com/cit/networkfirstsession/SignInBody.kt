package com.cit.networkfirstsession

data class SignInBody(
    val email: String,
    val password: String
)