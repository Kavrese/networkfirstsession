package com.cit.networkfirstsession

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.DatePicker
import android.widget.PopupMenu
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.Calendar

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        card_sex.setOnClickListener {
            val popup = PopupMenu(this, card_sex)
            popup.inflate(R.menu.sex_menu)
            popup.setOnMenuItemClickListener {
                sex.text = it.title
                return@setOnMenuItemClickListener true
            }
            popup.show()
        }

        card_date_birth.setOnClickListener {
            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            val picker = DatePickerDialog(this, object: OnDateSetListener{
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    val realMonth = month + 1

                    val textMonth = if (realMonth < 10)
                        "0$realMonth"
                    else
                        realMonth.toString()

                    val textDay = if (dayOfMonth < 10)
                        "0$dayOfMonth"
                    else
                        dayOfMonth.toString()

                    val text = "$textDay.${textMonth}.$year"
                    date_birth_day.text = text
                }
            }, year, month, day)
            picker.show()
        }

        reg.setOnClickListener {
            val firstname = firstname.text.toString()
            val lastname = lastname.text.toString()
            val patronymic = patronymic.text.toString()
            val email = email.text.toString()
            val sex = sex.text.toString()
            val dateBirthDay = date_birth_day.text.toString()
            val password = password.text.toString()

            if (firstname.isEmpty()){
                Toast.makeText(this, "Имя не должно быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (lastname.isEmpty()){
                Toast.makeText(this, "Фамилия не должно быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (patronymic.isEmpty()){
                Toast.makeText(this, "Отчество не должно быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (email.isEmpty()){
                Toast.makeText(this, "Почта не должна быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (sex.isEmpty()){
                Toast.makeText(this, "Пол не должен быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (dateBirthDay.isEmpty()){
                Toast.makeText(this, "Дата рождения не должна быть пустой !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (password.isEmpty()){
                Toast.makeText(this, "Пароль не должен быть пустой !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "Почта не соотвествует паттерну!", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val body = SignUpBody(firstname, lastname, patronymic, email, password, dateBirthDay, sex)
            Connection.api.signUp(body).enqueue(object: Callback<ModelToken> {
                override fun onResponse(call: Call<ModelToken>, response: Response<ModelToken>) {
                    if (response.body() == null) {
                        if (response.errorBody() == null) {
                            Toast.makeText(this@SignUpActivity, "Body null", Toast.LENGTH_LONG).show()
                        }else{
                            try {
                                val modelError = Gson().fromJson(
                                    response.errorBody()!!.string(),
                                    ModelError::class.java
                                )
                                Toast.makeText(this@SignUpActivity, modelError.error, Toast.LENGTH_LONG).show()
                            }catch (e: Exception){
                                Toast.makeText(this@SignUpActivity, "Body null", Toast.LENGTH_LONG).show()
                            }
                        }
                        return
                    }

                    val modelToken = response.body()!!
                    val intent = Intent(this@SignUpActivity, MainActivity::class.java)
                    intent.putExtra("token", modelToken.token)
                    startActivity(intent)
                    finish()
                }

                override fun onFailure(call: Call<ModelToken>, t: Throwable) {
                    Toast.makeText(this@SignUpActivity, t.message, Toast.LENGTH_LONG).show()
                }

            })
        }

        to_auth.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
            finish()
        }
    }
}