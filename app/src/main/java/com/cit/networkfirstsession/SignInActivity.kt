package com.cit.networkfirstsession

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sign_in.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        auth.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()

            if (password.isEmpty()){
                Toast.makeText(this, "Пароль не должен быть пустой !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (email.isEmpty()){
                Toast.makeText(this, "Почта не должна быть пустым !", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "Почта не соотвествует паттерну!", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val body = SignInBody(email, password)
            Connection.api.signIn(body).enqueue(object: Callback<ModelToken>{
                override fun onResponse(call: Call<ModelToken>, response: Response<ModelToken>) {
                    if (response.body() == null) {
                        if (response.errorBody() == null) {
                            Toast.makeText(this@SignInActivity, "Body null", Toast.LENGTH_LONG).show()
                        }else{
                            try {
                                val modelError = Gson().fromJson(
                                    response.errorBody()!!.string(),
                                    ModelError::class.java
                                )
                                Toast.makeText(this@SignInActivity, modelError.error, Toast.LENGTH_LONG).show()
                            }catch (e: Exception){
                                Toast.makeText(this@SignInActivity, "Body null", Toast.LENGTH_LONG).show()
                            }
                        }
                        Toast.makeText(this@SignInActivity, "Body null", Toast.LENGTH_LONG).show()
                        return
                    }

                    val modelToken = response.body()!!
                    val intent = Intent(this@SignInActivity, MainActivity::class.java)
                    intent.putExtra("token", modelToken.token)
                    startActivity(intent)
                    finish()
                }

                override fun onFailure(call: Call<ModelToken>, t: Throwable) {
                    Toast.makeText(this@SignInActivity, t.message, Toast.LENGTH_LONG).show()
                }
            })
        }

        create_accout.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            finish()
        }
    }
}