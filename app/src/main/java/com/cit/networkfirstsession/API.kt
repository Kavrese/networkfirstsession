package com.cit.networkfirstsession

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface API {
    @POST("signIn")
    fun signIn(@Body signInBody: SignInBody): Call<ModelToken>

    @POST("signUp")
    fun signUp(@Body signUpBody: SignUpBody): Call<ModelToken>
}