package com.cit.networkfirstsession

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Connection {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("http://strukov-artemii.online:8085/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api: API = retrofit.create(API::class.java)
}